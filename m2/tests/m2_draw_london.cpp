#include <iostream>
#include <string>
#include <unittest++/UnitTest++.h>

#include "unit_test_util.h"

#include "m1.h"
#include "m2.h"


TEST(TestDrawLondon) {
    bool load_success = false;
    {
        ECE297_TIME_CONSTRAINT(20000);

        load_success = load_map("/cad2/ece297s/public/maps/london_england.streets.bin");

    }
    CHECK(load_success);

    draw_map();

    close_map();
}
