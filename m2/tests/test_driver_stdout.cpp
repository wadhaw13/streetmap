#include <iostream>
#include <string>
#include <unittest++/UnitTest++.h>

int main() {
    std::cout << "Click 'Proceed' in the graphics window to finish the test" << std::endl;
    //Run the unit tests
    int num_failures = UnitTest::RunAllTests();

    return num_failures;
}
