#include <random>
#include <iostream>
#include <unittest++/UnitTest++.h>

#include "StreetsDatabaseAPI.h"
#include "m1.h"
#include "m3.h"
#include "m4.h"

#include "unit_test_util.h"
#include "courier_verify.h"

using ece297test::relative_error;
using ece297test::courier_path_is_legal_with_capacity;


SUITE(simple_legality_new_york_usa_public) {
    TEST(simple_legality_new_york_usa) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        std::vector<CourierSubpath> result_path;
        float turn_penalty;
        float truck_capacity;

        deliveries = {DeliveryInfo(36470, 43469, 156.64452)};
        depots = {9};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(30659, 49326, 186.19882), DeliveryInfo(44849, 72971, 2.90214), DeliveryInfo(84922, 104664, 89.01582)};
        depots = {21433, 100828, 207819};
        turn_penalty = 15.000000000;
        truck_capacity = 6185.435058594;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(48913, 37616, 60.36134), DeliveryInfo(109374, 128213, 37.21818)};
        depots = {52432, 74050};
        turn_penalty = 15.000000000;
        truck_capacity = 11899.549804688;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(27226, 108551, 146.33839), DeliveryInfo(82603, 136839, 100.59782), DeliveryInfo(140865, 13961, 157.35182)};
        depots = {70633, 17736, 74478};
        turn_penalty = 15.000000000;
        truck_capacity = 9747.506835938;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(35391, 102403, 174.61108), DeliveryInfo(109412, 213690, 52.14658), DeliveryInfo(97636, 80781, 167.38791), DeliveryInfo(108547, 98757, 182.28844), DeliveryInfo(141157, 174721, 45.65729)};
        depots = {54510, 191175, 101357};
        turn_penalty = 15.000000000;
        truck_capacity = 6967.588867188;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(39633, 40157, 115.91381), DeliveryInfo(46957, 185394, 75.65656)};
        depots = {132295};
        turn_penalty = 15.000000000;
        truck_capacity = 1328.184082031;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(81357, 6423, 159.20335)};
        depots = {200704};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(97021, 200127, 4.68502), DeliveryInfo(127426, 182731, 150.65356)};
        depots = {35268};
        turn_penalty = 15.000000000;
        truck_capacity = 14852.536132812;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(102639, 134729, 175.53738)};
        depots = {50050, 33943};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(174693, 25687, 58.09481), DeliveryInfo(76597, 97410, 84.76921), DeliveryInfo(168848, 97410, 128.60576), DeliveryInfo(174693, 105358, 94.46448), DeliveryInfo(168848, 105358, 128.41371), DeliveryInfo(124480, 97410, 94.63832), DeliveryInfo(174693, 33529, 94.87312), DeliveryInfo(174693, 105358, 94.46448), DeliveryInfo(71763, 108431, 20.23751)};
        depots = {72181, 121887, 20617};
        turn_penalty = 15.000000000;
        truck_capacity = 6465.099121094;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(120248, 36013, 72.53727)};
        depots = {82287, 95904};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(104441, 38406, 24.15597)};
        depots = {112759};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(189476, 15997, 1.66533), DeliveryInfo(200472, 33889, 69.64507)};
        depots = {55240, 74852};
        turn_penalty = 15.000000000;
        truck_capacity = 2802.345214844;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(29757, 44898, 188.57120), DeliveryInfo(128139, 143750, 120.73338), DeliveryInfo(82256, 88589, 120.28966), DeliveryInfo(85248, 143750, 120.91861), DeliveryInfo(148977, 205345, 99.21876), DeliveryInfo(176347, 143750, 62.35834), DeliveryInfo(211865, 60599, 163.40495), DeliveryInfo(110904, 60599, 67.73763)};
        depots = {136837, 84279, 186394};
        turn_penalty = 15.000000000;
        truck_capacity = 7671.847167969;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(53650, 64880, 158.54733), DeliveryInfo(38108, 192515, 38.37209), DeliveryInfo(17428, 18272, 59.23335)};
        depots = {116539, 84900, 116008};
        turn_penalty = 15.000000000;
        truck_capacity = 3976.689208984;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(25031, 77122, 46.84510), DeliveryInfo(158761, 57326, 59.81778), DeliveryInfo(195718, 205973, 64.17639), DeliveryInfo(79619, 184981, 58.65097), DeliveryInfo(47586, 108938, 141.06827)};
        depots = {147723, 153025, 8881};
        turn_penalty = 15.000000000;
        truck_capacity = 5195.080566406;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(146514, 87125, 13.91874), DeliveryInfo(194554, 14669, 8.56278), DeliveryInfo(146514, 88900, 41.87980), DeliveryInfo(100827, 155268, 71.50895), DeliveryInfo(146514, 204951, 179.63609), DeliveryInfo(21900, 136233, 20.90369), DeliveryInfo(21900, 188759, 95.92138), DeliveryInfo(21216, 162830, 134.08324)};
        depots = {187148, 207505, 48811};
        turn_penalty = 15.000000000;
        truck_capacity = 9899.384765625;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(187728, 116390, 122.03896), DeliveryInfo(150290, 93017, 142.83829), DeliveryInfo(171159, 6604, 146.78773), DeliveryInfo(134716, 170731, 190.66869), DeliveryInfo(119115, 39975, 168.13336)};
        depots = {182709, 164504, 5782};
        turn_penalty = 15.000000000;
        truck_capacity = 14417.934570312;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(199698, 77635, 137.59500), DeliveryInfo(194811, 200082, 59.62290), DeliveryInfo(38414, 77635, 15.90577), DeliveryInfo(199698, 178475, 187.31200), DeliveryInfo(38414, 200082, 46.18541), DeliveryInfo(199698, 77635, 137.59500), DeliveryInfo(94431, 200082, 63.51763), DeliveryInfo(199698, 156551, 48.40664), DeliveryInfo(179672, 41509, 36.90132)};
        depots = {159618, 54055, 133042};
        turn_penalty = 15.000000000;
        truck_capacity = 4274.875000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(204081, 188623, 147.03349), DeliveryInfo(204081, 73677, 83.66404), DeliveryInfo(3038, 121286, 24.86163), DeliveryInfo(98079, 121286, 55.82699), DeliveryInfo(204081, 77353, 53.61071), DeliveryInfo(138395, 188623, 156.97383), DeliveryInfo(138395, 121286, 9.75238), DeliveryInfo(204081, 188623, 147.03349), DeliveryInfo(166085, 171303, 42.47386)};
        depots = {38571, 62103, 39963};
        turn_penalty = 15.000000000;
        truck_capacity = 7168.385742188;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(211180, 117022, 28.66476)};
        depots = {42696, 147938};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(116922, 118671, 32.81808), DeliveryInfo(46724, 64734, 161.50987), DeliveryInfo(82490, 30602, 43.20779), DeliveryInfo(78643, 61052, 82.98276), DeliveryInfo(106001, 53987, 60.49590)};
        depots = {107350, 12409, 87630};
        turn_penalty = 15.000000000;
        truck_capacity = 10622.385742188;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(132964, 125871, 61.16725), DeliveryInfo(125507, 25699, 113.45358), DeliveryInfo(194046, 1789, 28.82484), DeliveryInfo(206976, 27294, 4.02114), DeliveryInfo(199203, 95306, 104.51654), DeliveryInfo(194046, 148880, 117.75246), DeliveryInfo(125507, 43511, 165.01239), DeliveryInfo(194046, 176466, 3.76336)};
        depots = {120266, 63285, 36600};
        turn_penalty = 15.000000000;
        truck_capacity = 8867.809570312;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(92492, 197889, 176.74138), DeliveryInfo(202485, 68244, 93.27872), DeliveryInfo(195117, 147326, 72.90471), DeliveryInfo(128277, 197889, 79.35565), DeliveryInfo(91716, 147326, 82.74268), DeliveryInfo(132, 199032, 157.75711), DeliveryInfo(155140, 36051, 182.96613), DeliveryInfo(202919, 147326, 71.61387)};
        depots = {128606, 106894, 59192};
        turn_penalty = 15.000000000;
        truck_capacity = 4273.869628906;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(155251, 137156, 166.39528), DeliveryInfo(169229, 107887, 6.80600), DeliveryInfo(155251, 104095, 101.82253), DeliveryInfo(140755, 70902, 14.59690), DeliveryInfo(174290, 100850, 47.61852), DeliveryInfo(174290, 173120, 193.32730), DeliveryInfo(28642, 74807, 66.61172), DeliveryInfo(155251, 75482, 75.14173)};
        depots = {131795, 96855, 135764};
        turn_penalty = 15.000000000;
        truck_capacity = 9917.281250000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(162080, 153529, 179.91382), DeliveryInfo(104867, 142192, 189.80901), DeliveryInfo(14871, 123725, 160.49899), DeliveryInfo(20305, 153529, 51.18974), DeliveryInfo(7805, 185411, 163.61295), DeliveryInfo(75501, 197086, 20.06098), DeliveryInfo(9934, 153529, 70.15913), DeliveryInfo(89267, 142192, 121.61732)};
        depots = {180274, 173213, 49946};
        turn_penalty = 15.000000000;
        truck_capacity = 3549.154541016;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(174494, 126106, 184.61324), DeliveryInfo(187103, 177839, 65.88518)};
        depots = {171343};
        turn_penalty = 15.000000000;
        truck_capacity = 10739.620117188;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(206507, 145424, 66.27128), DeliveryInfo(50941, 142208, 180.74861), DeliveryInfo(46617, 78871, 115.90621), DeliveryInfo(96165, 67392, 108.68627), DeliveryInfo(132853, 95299, 194.95090)};
        depots = {45337, 36675, 80985};
        turn_penalty = 15.000000000;
        truck_capacity = 12754.851562500;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(192807, 174019, 193.41937), DeliveryInfo(176559, 86195, 146.50169), DeliveryInfo(17737, 152761, 183.47961), DeliveryInfo(141249, 100640, 144.07825), DeliveryInfo(118782, 53705, 0.80647)};
        depots = {120585, 12666, 36264};
        turn_penalty = 15.000000000;
        truck_capacity = 11383.201171875;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(211147, 33049, 65.56896), DeliveryInfo(85145, 143615, 182.31299)};
        depots = {176698, 175371};
        turn_penalty = 15.000000000;
        truck_capacity = 2286.546142578;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

    } //simple_legality_new_york_usa

} //simple_legality_new_york_usa_public

