import sys
import subprocess
import os
import os.path as path
import pwd
import errno
import signal
import re
import urllib.request, urllib.parse, urllib.error
import math
import shutil
import codecs;
import locale

#For coloured terminal text
from .. import colorama

from ece297test.errors import ConfigError, TimeOutError, ActionFailedError

#Intialize colorama and setup some useful style codes
class TermStyle:
    #Terminal styles, default to off
    BOLD = ''
    GREEN = ''
    RED = ''
    YELLOW = ''
    PASS = ''
    FAIL = ''
    RESET = ''

    @classmethod
    def enable(self):
        colorama.init()
        self.BOLD = colorama.Style.BRIGHT
        self.GREEN = colorama.Back.GREEN + colorama.Fore.BLACK;
        self.RED = colorama.Back.RED + colorama.Fore.BLACK;
        self.YELLOW = colorama.Back.YELLOW + colorama.Fore.BLACK;

        self.PASS = self.BOLD + self.GREEN
        self.FAIL = self.BOLD + self.RED
        self.WARN = self.BOLD + self.YELLOW
        self.SKIP = self.WARN
        self.RESET = colorama.Style.RESET_ALL

    @classmethod
    def disable(self):
        self.BOLD = ""
        self.GREEN = ""
        self.RED = ""
        self.PASS = ""
        self.FAIL = ""
        self.WARN = ""
        self.SKIP = ""
        self.RESET = ""

#Use absolute paths, so we don't rely on students
# environment (which could be broken)
TIMEOUT_EXEC = "/usr/bin/timeout"
TIMEOUT_TIMEDOUT_RETVAL = 124
TIMEOUT_COMMAND_NOT_FOUND_RETVAL = 127

def get_param(config, section, param, defaults=None, required=True):
    if not defaults:
        defaults = {}

    if config.has_option(section, param):
        #Specified
        return config.get(section, param)

    elif param in defaults:
        #Default
        return defaults[param]
    elif not required:
        #Missing but optional
        return None

    else:
        #Missing but required
        raise ConfigError("Missing required parameter {} in section {} (not explicitly set or defaulted)".format(param, section))

def run_essential_command(*args, **kwargs):
    kwargs.setdefault("show_action_name", True);
    kwargs.setdefault("throw_on_fail", True);
    return run_command(*args, **kwargs);

def run_command(cmd, action_name, timeout=None, show_cmd=False, live_output=True, f=sys.stdout, indent='', throw_on_fail=False, show_action_name=False, cwd='.'):
    """
    Run executes the command specified in the cmd list.

    action_name: name used for error reporting
    timeout: Time in seconds before killing the job
    show_cmd: print the command being run (for debug)
    live_output: print command output as it comes in rather than buffering and printing all output when command finishes
    f: Output to print to
    indent: String to prefix all output by
    """

    # write UTF-8 no matter the user's locale. This is the encoding that the tests use
    f = codecs.getwriter('utf-8')(f.buffer, 'strict')

    if timeout != None:
        cmd = [TIMEOUT_EXEC, "%ss" % timeout] + cmd

    if show_action_name:
        print(indent + "===> " + action_name + "...", file=f);

    if show_cmd:
        print(indent + ' '.join(cmd), file=f)

    #Flush output before calling subprocess
    # This ensures the output is in order 
    # if stdout is buffered
    f.flush()

    #Default
    output = None
    returncode = 0
    proc = None

    # Popen in python3 < 3.6 doesn't have the encoding option
    # so save, set and set to saved
    old_locale = locale.getlocale(locale.LC_CTYPE);
    if old_locale[1] != "UTF-8":
        print("DEBUG: original non-UTF-8 locale in run_command is {}".format(old_locale))

    try:
        locale.setlocale(locale.LC_CTYPE, (None, 'utf-8'))
        proc = subprocess.Popen(cmd, #Command to run
                                stdout=subprocess.PIPE, #We grab stdout
                                stderr=subprocess.STDOUT, #stderr goes to stdout
                                universal_newlines=True, #Lines always end in \n
                                cwd=cwd,
                                )


        output = []
        while proc.poll() is None:
            data = proc.stdout.readline() #Blocks until new line
            if data != "": #We sometimes get empty lines back without a \n, ignore them
                lines = data.split('\n') #Some times get mulitple lines at-a-time
                if data[-1] == '\n':
                    lines = lines[:-1] #Drop the extra 'blank' if the data ended in \n
                output += lines
                if live_output:
                    for line in lines:
                        print(indent + line, file=f)
                    f.flush()

        #Get any unflushed input
        data = proc.stdout.read()
        if data != "":
            lines = data.split('\n') #Some times get mulitple lines at-a-time
            if data[-1] == '\n':
                lines = lines[:-1] #Drop the extra 'blank' if the data ended in \n
            output += lines
            if live_output:
                for line in lines:
                    print(indent + line, file=f)
                f.flush()

    finally:
        #Make sure we clean up if we didn't exit cleanly
        if proc and proc.returncode == None:
            #Still running stop it
            proc.terminate()
        old_locale = locale.setlocale(locale.LC_CTYPE, old_locale);


    if timeout and proc.returncode == TIMEOUT_TIMEDOUT_RETVAL:
        raise TimeOutError("Run-time exceeded overall command timeout of {time}s".format(time=timeout), output)

    if throw_on_fail and proc.returncode != 0:
        raise ActionFailedError("Action `{}': `{}' exited with non-zero exit code {}. The last few lines of output were:\n\n{}\n".format(
            action_name, ' '.join(cmd), proc.returncode, '\n'.join(output[-5:])), remove_newlines=False);

    return output, proc.returncode

def get_user_name():
    #Note we explicitly look up the user based on the UserID
    # rather than rely on environment variables (which could
    # be manipulated)
    return pwd.getpwuid(os.getuid()).pw_name

def valid_file(file_path):
    return path.isfile(file_path) and path.getsize(file_path) > 0

def find_config_file(config_dir, milestone_number):
    """
    Search for the config file for milestone_number
    """

    config_file_path = path.join(config_dir, "m%d" % milestone_number, "milestone_%d.cfg" % milestone_number)

    if not path.isfile(config_file_path):
        raise ConfigError("Could not find config file {config}!".format(config=path.basename(config_file_path)))

    return config_file_path

def object_file_build_type(file_path):
    """
    Determine whether the object file is from a debug or release build
    """

    if object_file_uses_glibcxx_debug(file_path):
        return "debug_check"
    else:
        return "release"

def object_file_uses_glibcxx_debug(file_path):
    stl_containers = ["vector", "deque", "map", "set", "unordered_map", "unordered_set"]

    glibcxx_debug_string_patterns = []

    for container in stl_containers:
        glibcxx_debug_string_patterns.append(r"__debug\d+" + container)
        glibcxx_debug_string_patterns.append(r"std::__debug::" + container)

    safe_objects = ["_Safe_sequence_", "_Safe_unordered_container", "_Safe_iterator", "_Safe_iterator_base", "_Safe_local_iterator"]

    for safe_obj in safe_objects:
        glibcxx_debug_string_patterns.append(r"__gnu_debug\d+" + safe_obj)
        glibcxx_debug_string_patterns.append(r"__gnu_debug::" + safe_obj)

    grep_pattern = "|".join(glibcxx_debug_string_patterns)

    return count_object_strings(file_path, grep_pattern) > 0


def object_file_uses_ubsan(file_path):
    grep_pattern = "__ubsan"

    return count_object_strings(file_path, grep_pattern) > 0

def object_file_uses_asan(file_path):
    grep_pattern = "__asan"

    return count_object_strings(file_path, grep_pattern) > 0

def count_object_strings(file_path, grep_pattern):
    cmd = "strings {file} | grep -P '{pattern}' | wc -l".format(file=file_path, pattern=grep_pattern)
    output = subprocess.check_output(cmd, shell=True)

    output = output.decode('utf-8').split('\n')

    #Should be a single number
    assert len(output) == 2, "output is not single number: {}".format(output);
    num_refs = int(output[0])

    return num_refs

def determine_custom_link_flags(makefile_path, target_name="echo_flags"):
    cmd = "make -f {} {}".format(makefile_path, target_name);
    try:
        output = subprocess.check_output(cmd, shell=True)
    except subprocess.CalledProcessError as e:
        raise ActionFailedError("Failed to determine custom link flags. Make sure your make file supports the '{}' target!".format(target_name), e)


    output = output.decode('utf-8').split('\n')

    link_flags_regex = re.compile(r"CUSTOM_LINK_FLAGS: (?P<link_flags>.*)")

    for line in output:
        match = link_flags_regex.match(line)
        if match:
            return match.group("link_flags").split()

    # compat with older makefiles
    if target_name != "custom_flags":
        return determine_custom_link_flags(makefile_path, "custom_flags");

    return []

def mkdir_p(directory_path):
    """
    Make directory path including parents (like mkdir -p)
    """
    try:
        os.makedirs(directory_path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(directory_path):
            pass
        else: raise

def prompt_user_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "Y": True,
             "no": False, "n": False, "N": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no'\n")

def prompt_user_question(question, answer):
    while True:
        sys.stdout.write(question)
        choice = input().lower()
        if choice == answer:
            return True
        else:
            sys.stdout.write("Invalid response (expected {})\n".format(answer))

    return False #Unreachable
    
#def interpret_subprocess_exit_code(exit_code, expected_exit_code=None):
    #"""
    #Return a useful interpretation of the given exit code,
    #returned by a call to subprocess.
    #"""
    #msg = ""
    #if(exit_code < 0):
        ##subprocess returns signals as negative values
        #sig_num = -1*exit_code

        ##Buld the dictionary of signal numbers to names
        #sig_num_to_name = dict((k, v) for v, k in reversed(sorted(signal.__dict__.items())) if v.startswith('SIG') and not v.startswith('SIG_'))

        #if sig_num in sig_num_to_name:
            #sig_name = sig_num_to_name[sig_num]
            #msg += sig_name

            ##Try and give the students some guidance on what these mean
            #if sig_name == "SIGSEV":
                #msg += " (Segmentation Fault). Possible causes: accessed invalid memory."
            #elif sig_name == "SIGABRT":
                #msg += " (Abort). Possible causes: memory corruption, assertion failure, other fatal errors."
            #else:
                #msg += "."

    #msg += " Exit code was: " + str(exit_code)

    ##Show the expected code if any
    #if expected_exit_code != None:
        #assert exit_code != expected_exit_code
        #msg += " (expected: " + str(expected_exit_code) + ")"


    #return msg

def get_url(url, target_path):
    """
    Download a URL or copy a file to target_path
    """

    if url.startswith("http"):
        download_url(url, target_path)
    else: #Assume direct file path
        print("Copying {}".format(url))
        shutil.copy(url, target_path)

def download_url(url, target_path):
    """
    Downloads a file from a URL
    """
    print("Downloading {}".format(url))
    urllib.request.urlretrieve(url, target_path, reporthook=download_progress_callback)

    return target_path

def download_progress_callback(block_num, block_size, expected_size):
    """
    Callback for urllib.urlretrieve which prints a dot for every percent of a file downloaded
    """
    total_blocks = int(math.ceil(expected_size / float(block_size)))
    progress_increment = int(math.ceil(total_blocks / 100.))

    if block_num % progress_increment == 0:
        sys.stdout.write(".")
        sys.stdout.flush()
    if block_num*block_size >= expected_size:
        print("")

