import re
import os
import os.path as path
import xml.etree.ElementTree as ET

from ece297test.util import run_command, valid_file, object_file_uses_glibcxx_debug, determine_custom_link_flags, object_file_uses_ubsan, object_file_uses_asan, get_param
from ece297test.errors import ConfigError, BuildError, ActionFailedError, CrashError
import ece297test.base as ece297base


class UnitTester(ece297base.BuildRunParseTester):
    """
    Tester for running unit tests
    """
    def __init__(self, config, section, defaults):
        #To avoid directly evaluating the configuration file
        #input to determine classes, we instead use a dictionary
        #to map between them
        self.builders = {'UnitTestBuilder': UnitTestBuilder}
        self.runners = {'UnitTestRunner': UnitTestRunner, 'UnitTestXmlRunner': UnitTestXmlRunner}
        self.parsers = {'UnitTestParser': UnitTestParser, 'UnitTestXmlParser': UnitTestXmlParser, 'UnitTestXmlQorParser': UnitTestXmlQorParser}

        #The base class will query the builders/runners/parsers dictionaries above, and
        #initialize the appropriate objects.  Therefore the call to the base class
        #must happen AFTER the Make the dictionaries have been initialized
        super(UnitTester, self).__init__(config, section, defaults)

class UnitTestBuilder(ece297base.Builder):
    """
    Builder for compiling unit tests and linking to student's libraries
    """
    def __init__(self, config, section, defaults):
        super(UnitTestBuilder, self).__init__(config, section, defaults)
        self.makefile_path = get_param(config, 'Milestone', 'makefile_name')
        self.static_lib_file_paths = get_param(config, 'Milestone', 'static_library_file_names').split() 
        self.header_dir_paths = get_param(config, 'Milestone', 'static_library_header_dirs').split() 
        self.unit_test_driver_file_path = get_param(config, section, 'unit_test_driver', defaults)
        self.unit_test_file_path_list = get_param(config, section, 'unit_tests', defaults).split()  
        self.compile_options = get_param(config, section, 'compile_options', defaults).split()
        self.link_debug_options = get_param(config, section, 'link_debug_options', defaults).split()
        self.link_release_options = get_param(config, section, 'link_release_options', defaults).split()

        self.executable_file_path = get_param(config, section, 'executable_file_path', defaults, required=False)

        if not self.executable_file_path:
            #Derive from section name
            self.executable_file_path = section

    def build(self):

        cmd = ['/usr/bin/g++']

        cmd += self.compile_options
        for header_dir_path in self.header_dir_paths:
            cmd += ['-I', header_dir_path] #Library header

        unit_test_include_dirs = {}
        for file_path in [self.unit_test_driver_file_path] + self.unit_test_file_path_list:
            dir_path = path.dirname(file_path)
            if dir_path not in unit_test_include_dirs:
                unit_test_include_dirs[dir_path] = True

        for dir_path in list(unit_test_include_dirs.keys()):
            cmd += ['-I', dir_path]

        cmd += [self.unit_test_driver_file_path] #File including main() which drives unit tests
        cmd += self.unit_test_file_path_list #Files containing unit tests

        #Check each static library and record its build type
        static_lib_uses_glibcxx_debug = {}
        asan_in_use = False
        ubsan_in_use = False

        #Search path for static library
        for lib_path in self.static_lib_file_paths:
            static_lib_dir = path.dirname(lib_path)
            if static_lib_dir == "": #If we are in the directory, we need to add dot to the search path
                static_lib_dir = "."
            cmd += ['-L', static_lib_dir] 

            #Determine the name of the library.
            #  g++ searches the library path for files matching 'libname.a', where 'name' is the library name
            #  it expects the 'lib' prefix and '.a' suffix
            # 
            # When we link we need to specify 'name' to g++

            #Split of file extension and name
            static_lib_file_name, file_extension = path.splitext(path.basename(lib_path))

            #Confirm that it matches the expected naming scheme
            if file_extension != '.a':
                raise ConfigError("Static library {lib} does not have file extension '.a'".format(lib=lib_path))

            if not static_lib_file_name.startswith('lib'):
                raise ConfigError("Static library {lib} filename does not start with 'lib'".format(lib=lib_path))

            #Verify the build type
            if object_file_uses_glibcxx_debug(lib_path):
                static_lib_uses_glibcxx_debug[lib_path] = True
            else:
                static_lib_uses_glibcxx_debug[lib_path] = False
            if object_file_uses_asan(lib_path):
                asan_in_use = True
            if object_file_uses_ubsan(lib_path):
                ubsan_in_use = True

            #Strip off the starting 'lib'
            assert static_lib_file_name.startswith('lib')
            library_name = static_lib_file_name[3:]

            cmd += ['-l', library_name] #Static library to link against

        if len(set(static_lib_uses_glibcxx_debug.values())) != 1:
            raise ConfigError("Mixed build and release static libraries observed: {values} Please use a single build type.".format(values=static_lib_build_type))

        #We only have a single build type at this point
        if True in list(static_lib_uses_glibcxx_debug.values()):
            #We need to do a debug build
            #
            #Note that we must provide this flag otherwise we will run into binary compatibility issues
            #between the debug and release builds!
            cmd += ["-D_GLIBCXX_DEBUG"]
            cmd += self.link_debug_options
        else:
            cmd += self.link_release_options

        if asan_in_use:
            cmd += ['-fsanitize=address', '-fsanitize=leak']
            
        if ubsan_in_use:
            cmd += ['-fsanitize=undefined']

        cmd += determine_custom_link_flags(self.makefile_path)

        cmd += ['-o', self.executable_file_path] #Output name

        #Call g++ to build the test executable
        output, returncode = run_command(cmd, "Building UnitTests", timeout=self.timeout)

        if returncode != 0:
            raise BuildError("Failed to build unit tests. Compiler returned non-zero exit status {status}".format(status=returncode))


        #Verify that the test executable was created
        if not valid_file(self.executable_file_path):
            raise BuildError("Could not find UnitTest binary.")


        #Return the abspath of the exec
        return path.abspath(self.executable_file_path)

class UnitTestRunner(ece297base.Runner):
    """
    Default runner
    """
    pass

class UnitTestXmlRunner(ece297base.Runner):
    """
    Unit test runner which passes XML output file as first argument to executable
    """
    def __init__(self, config, section, defaults):
        super(UnitTestXmlRunner, self).__init__(config, section, defaults)

class UnitTestParser(ece297base.Parser):
    """
    Standard output unit test parser
    """
    def parse(self, name, test_output, test_error_code):
        num_tests = 0
        num_failed = 0

        #Assuming standard UnitTest++ output, this is the final summary line if tests failed
        summary_fail_regex = re.compile(r"FAILURE:\s+(?P<failed_tests>\d+)\s+out of\s+(?P<total_tests>\d+)\s+tests failed \(\s*(?P<failures>\d+)\s+failures\)")
        summary_pass_regex = re.compile(r"Success:\s+(?P<passed_tests>\d+)\s+tests\s+passed.")
        if test_output:
            for line in test_output:
                line = line.strip()
                match = summary_fail_regex.match(line)
                if match:
                    num_tests = int(match.group('total_tests'))
                    num_failed = int(match.group('failed_tests'))
                    break
                match = summary_pass_regex.match(line)
                if match:
                    num_tests = int(match.group("passed_tests"))
                    num_failed = 0
                    break

        if test_error_code != 0 and num_failed <= 0:
            raise CrashError(exit_code=test_error_code, expected_exit_code=0, message = "Could not parse tests.")

        #Build the test result output
        tester_result = ece297base.TesterResult(name)
        for i in range(0,num_tests):
            #Since we only parse the summary, we don't know the details
            test_result = ece297base.TestResult("Unknown Unit Test {num}".format(num=i))
            if i < num_failed:
                test_result.add_failure()
            tester_result.add_test_result(test_result)

        return tester_result

class UnitTestXmlParser(ece297base.Parser):
    """
    Parses the UnitTest++ XML output generated by the UnitTestXmlRunner
    """
    def __init__(self, config, section, defaults):
        super(UnitTestXmlParser, self).__init__(config, section, defaults)

        xml_base = get_param(config, section, 'executable_file_path', defaults, required=False)
        if not xml_base:
            xml_base = section

        self.xml_file = xml_base + '.xml'

    def parse(self, name, test_output, test_error_code):
        tester_result = ece297base.TesterResult(name)

        try:
            #Parse the xml file generated by the unit test framework
            tree = ET.parse(self.xml_file)
            root = tree.getroot()
            assert root.tag == 'unittest-results', "Root Tag was: " + root.tag

            for test_node in root:
                #Should be only test nodes below root
                assert test_node.tag == 'test', "test tag was: " + test_node.tag

                test_result = ece297base.TestResult(test_name=test_node.get('name'), test_time=float(test_node.get('time')))

                for failure_node in test_node:
                    assert failure_node.tag == 'failure', "failure tag was: " + failure_node.tag
                    test_result.add_failure()

                tester_result.add_test_result(test_result)
        except ET.ParseError as e:
            raise ActionFailedError(child_exception=e,
                             message="Could not parse xml test output (" + str(e) + ").")
        except IOError as e:
            raise ActionFailedError(child_exception=e,
                             message="Could not access file (" + str(e) + ").")

        return tester_result

class UnitTestXmlQorParser(ece297base.Parser):
    """
    Parses both the UnitTest++ XML output, and checks for QoR values printed
    by the unit tests
    """
    def __init__(self, config, section, defaults):
        super(UnitTestXmlQorParser, self).__init__(config, section, defaults)

        xml_base = get_param(config, section, 'executable_file_path', defaults, required=False)
        if not xml_base:
            xml_base = section

        self.xml_file = xml_base + '.xml'

    def parse(self, name, test_output, test_error_code):
        tester_result = ece297base.TesterResult(name)

        try:
            #Parse the xml file generated by the unit test framework
            tree = ET.parse(self.xml_file)
            root = tree.getroot()
            assert root.tag == 'unittest-results', "Root Tag was: " + root.tag

            for test_node in root:
                assert test_node.tag == 'test', "test tag was: " + test_node.tag
                test_name = test_node.get('name')

                #Find the QoR
                qor = self.parse_qor(test_name, test_output)

                test_result = ece297base.TestResult(test_name=test_name, test_time=float(test_node.get('time')), qor=qor)

                for failure_node in test_node:
                    assert failure_node.tag == 'failure', "failure tag was: " + failure_node.tag
                    test_result.add_failure()

                tester_result.add_test_result(test_result)
        except ET.ParseError as e:
            raise ActionFailedError(child_exception=e,
                             message="Could not parse xml test output (" + str(e) + ").")
        except IOError as e:
            raise ActionFailedError(child_exception=e,
                             message="Could not access file (" + str(e) + ").")

        return tester_result

    def parse_qor(self, test_name, test_output):

        cost_regex = re.compile(r".*QoR {key}: (?P<cost>([-+]?([0-9]*\.?[0-9]+|[0-9]+\.)([eE][-+]?[0-9]+)?)|INVALID)".format(key=test_name))

        qor = None
        for line in test_output:
            match = cost_regex.match(line)
            if match:
                if qor == None:
                    #First time seen match for this key
                    cost = match.group("cost")
                    try: 
                        qor = float(cost)
                    except ValueError as e:
                        if 'INVALID' == cost:
                            qor = cost #Set it as invalid
                        else:
                            raise InvalidResultError("Could not parse QoR for {test_name}".format(test_name=test_name), e)

                else:
                    raise InvalidResultError("Could not parse QoR: found duplicate cost for {test_name}".format(test_name=test_name))

        if qor == None:
            #raise ActionFailedError("Could not parse QoR did not find cost for {test_name}".format(test_name=test_name))
            qor = "N/A (not found)"

        return qor



        
