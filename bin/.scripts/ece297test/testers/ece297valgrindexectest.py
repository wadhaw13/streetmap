import os.path as path

import re

from ece297test.util import run_command, valid_file, object_file_uses_asan, get_param
from ece297test.errors import ConfigError, BuildError

from ece297test.base import BuildRunParseTester, TesterResult, TestResult, Builder, Runner, Parser
from ece297test.testers import UnitTestBuilder

class ValgrindExecTester(BuildRunParseTester):
    """
    Specialized ExecTester which runs the target executable through valgrind to detect
    memory errors and leaks.
    """
    def __init__(self, config, section, defaults):
        #To avoid directly evaluating the configuration file
        #input to determine classes, we instead use a dictionary
        #to map between them
        self.builders = {
                        'ValgrindExecBuilder': Builder, #Default
                        'UnitTestBuilder': UnitTestBuilder, #Run some unit tests
                      }
        self.runners = {'ValgrindExecRunner': ValgrindExecRunner}
        self.parsers = {'ValgrindExecParser': ValgrindExecParser}

        #The base class will query the builders/runners/parsers dictionaries above, and
        #initialize the appropriate objects.  Therefore the call to the base class
        #must happen AFTER the Make the dictionaries have been initialized
        super(ValgrindExecTester, self).__init__(config, section, defaults)


class ValgrindExecRunner(Runner):
    """
    Runner which wraps the executable with a call to valgrind
    """
    def __init__(self, config, section, defaults):
        super(ValgrindExecRunner, self).__init__(config, section, defaults)

        self.suppression_files = get_param(config, section, 'suppression_files', defaults, required=False)
        if self.suppression_files:
            self.suppression_files = self.suppression_files.split()
        else:
            self.suppression_files = []

    def run(self, output_f, indent, profiler=None):
        #Students may try to run valgrind tests while also having AddressSanitizer activated
        #It turns out you can not have both address sanitizer and valgrind active at the same
        #time, and doing so causes Valgrind to exit with an error
        #
        #To avoid confusing students we 'skip' running valgrind if AddressSanitizer is detected

        if object_file_uses_asan(self.exec_file_path):
            print("    Skipping Valgrind (Detected Address Sanitizer). To run valgrind tests you must use a non-'debug_check' build.", f=output_f)
            return ([], None)


        #At this point, we are sure that ASAN is not active

        #Build the valgrind command line
        cmd = ['/usr/bin/valgrind',
                '--tool=memcheck',
                '--leak-check=full',
                '--track-origins=yes',
                '--leak-check-heuristics=all',
                ]

        #Any configured suppression files must be added on the commandline
        for suppression_file in self.suppression_files:
            cmd += ["--suppressions={file}".format(file=suppression_file)]

        #The exectuable to run on
        cmd += [self.exec_file_path]
        cmd += self.exec_args

        return run_command(cmd, "Running Valgrind ExecTests", timeout=self.timeout, indent=2*indent, f=output_f)

class ValgrindExecParser(Parser):
    """
    Parser which checks for valgrind errors in output
    """
    def parse(self, name, test_output, test_exit_code):
        #Assuming standard valgrind output
        error_regexs = {
                        "Number of Invalid Reads                 ": re.compile(r"^\s*==\d+== Invalid read of size \d+"),
                        "Number of Invalid Writes                ": re.compile(r"^\s*==\d+== Invalid write of size \d+"),
                        "Number of Memory Leaks (definitely lost)": re.compile(r"^\s*==\d+== [\d,]+ (\(.*\) )?bytes in [\d,]+ blocks are definitely lost in loss record [\d,]+ of [\d,]+"),
                        "Number of Memory Leaks (indirectly lost)": re.compile(r"^\s*==\d+== [\d,]+ (\(.*\) )?bytes in [\d,]+ blocks are indirectly lost in loss record [\d,]+ of [\d,]+"),
                        "Number of Memory Leaks (possibly lost)  ": re.compile(r"^\s*==\d+== [\d,]+ (\(.*\) )?bytes in [\d,]+ blocks are possibly lost in loss record [\d,]+ of [\d,]+"),
                        "Number of Invalid free/delete/delete[]  ": re.compile(r"^\s*==\d+== (Mismatched|Invalid) free\(\)\s*/\s*delete\s*/\s*delete\s*\[\]"),
                       }
        num_tests = len(error_regexs)
        num_failed = 0
        failures_dict = {}
        for key in error_regexs:
            failures_dict[key] = 0

        for error_type in list(error_regexs.keys()):
            failures_dict[error_type] = 0

        for line in test_output:
            line = line.strip()
            for error_type, error_regex in error_regexs.items():
                match = error_regex.match(line)
                if match:
                    failures_dict[error_type] += 1

        tester_result = TesterResult(name)

        for error_type in sorted(failures_dict.keys()):
            error_count = failures_dict[error_type]
            test_result = TestResult(error_type)
            if error_count > 0:
                test_result.add_failure(
                    failure_count=error_count,
                    message = "{error_type}: {failed:2}".format(
                       error_type=error_type, failed=error_count
                    )
                )

            tester_result.add_test_result(test_result)

        if self.expected_exit_code != None:
            test_result = TestResult('Correct Exit Code')
            if test_exit_code != self.expected_exit_code:
                test_result.add_failure(
                    message="Expected exit code {expected}, but was {actual}. You may have functionality failures.".format(
                        actual=test_exit_code, expected=self.expected_exit_code
                    )
                )

            tester_result.add_test_result(test_result)

        return tester_result
