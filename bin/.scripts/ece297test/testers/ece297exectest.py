import sys
import os.path as path
import shutil
import configparser
from collections import OrderedDict

from ece297test.util import run_command, valid_file, get_param
from ece297test.errors import ConfigError, BuildError
from ece297test.base import BuildRunParseTester, TestResult, TesterResult, Builder, Runner, Parser

class ExecTester(BuildRunParseTester):
    """
    Basic executable tester.

    Assumes executable already exists.  It is simply run and its output parsed.
    """
    def __init__(self, config, section, defaults):
        #To avoid directly evaluating the configuration file
        #input to determine classes, we instead use a dictionary
        #to map between them
        self.builders = {'ExecBuilder': Builder}
        self.runners = {'ExecRunner': Runner}
        self.parsers = {'ExecParser': ExecParser}

        #The base class will query the builders/runners/parsers dictionaries above, and
        #initialize the appropriate objects.  Therefore the call to the base class
        #must happen AFTER the Make the dictionaries have been initialized
        super(ExecTester, self).__init__(config, section, defaults)

class ExecParser(Parser):
    """
    Custom parser for executable tests.  
    
    Checks: 
        1) That the correct exit code is returned
        2) That that strings listed in the file 'expected_strings_file' are found in the output.
    """
    def __init__(self, config, section, defaults):
        super(ExecParser, self).__init__(config, section, defaults)

        #Exected strings are optional
        self.expected_strings_file = get_param(config, section, 'expected_strings_file', defaults, required=False)

        #Blank means unused
        if self.expected_strings_file == "":
            self.expected_strings_file = None

    def parse(self, name, test_output, test_exit_code):
        #Collects test results
        tester_result = TesterResult(name)

        #Exit code check
        self.f = sys.stdout
        if self.expected_exit_code != None:
            expected_exit_code_test_result = TestResult("Correct Exit Code")
            print("    Correct Exit Code: ", end=' ', file=self.f)

            if test_exit_code != self.expected_exit_code:
                expected_exit_code_test_result.add_failure()
                print("No (expected {expected} was {actual})".format(actual=test_exit_code, expected=self.expected_exit_code), file=self.f)
            else:
                print("Yes".format(actual=test_exit_code, expected=self.expected_exit_code), file=self.f)

            #Save result
            tester_result.add_test_result(expected_exit_code_test_result)

        #Expected output check
        expected_lines, missing_lines = self.check_expected_strings(test_output)
        expected_output_test_result = TestResult("Found all expected output strings")

        print("    Found all expected output strings: ", end=' ', file=self.f)
        if len(missing_lines) == 0:
            print("Yes", file=self.f)
        else:
            expected_output_test_result.add_failure(failure_count=len(missing_lines))
            print("No", file=self.f)
            for line in missing_lines:
                print(" "*6 + "Missing '{line}' in output".format(line=line), file=self.f)

        #Save result
        tester_result.add_test_result(expected_output_test_result)

        return tester_result

    def check_expected_strings(self, test_output):
        num_failed = 0
        num_checks = 0
        expected_lines = []
        missing_lines = []
        if self.expected_strings_file != None:
            num_checks += 1
            with open(self.expected_strings_file) as f:
                expected_lines = f.readlines()

            expected_lines_dict = OrderedDict()
            for check_line in expected_lines:
                expected_lines_dict[check_line.strip()] = 0

            for output_line in test_output:
                for expected_line in list(expected_lines_dict.keys()):
                    if expected_line in output_line:
                        expected_lines_dict[expected_line] += 1

            missing_lines = [line for line, count in expected_lines_dict.items() if count == 0]

        return expected_lines, missing_lines




