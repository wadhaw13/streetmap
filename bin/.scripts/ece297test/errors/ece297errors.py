import signal

class ActionFailedError(Exception):
    """
    Error generated if some action failed (typically a run_command())
    """
    def __init__(self, message, child_exception=None, remove_newlines=True):

        if child_exception:

            reason_msg = str(child_exception)
            if (remove_newlines):
                reason_msg = reason_msg.replace("\n", " ")

            if len(reason_msg) != 0:
                message += " (reason: %s)" % reason_msg

        #Call base class constructor
        super(ActionFailedError, self).__init__(message)

class CrashError(Exception):
    """
    A problem with student code (i.e. killed by signal such as SIGSEV)
    """
    def __init__(self, message=None, exit_code=None, expected_exit_code=None):
        self.exit_code = exit_code
        self.expected_exit_code = expected_exit_code

        msg = ""
        if(exit_code != None and exit_code < 0):
            #Negative exit code implies killed by signal
            msg += "halted with "

            msg += self.sig_name()
            #Try and give the students some guidance on what these mean
            if self.sig_name() == "SIGSEGV":
                msg += " (Segmentation Fault). Likely causes: accessed invalid memory"
            elif self.sig_name() == "SIGABRT":
                msg += " (Abort). Likely causes: memory corruption, assertion failure"

        if message:
            if msg != "":
                msg += " "
            msg += message

        if self.exit_code != self.expected_exit_code:
            msg += self.describe_exit_code()

        super(CrashError, self).__init__(msg)

    def short_description(self):
        if self.exit_code < 0:
            return self.sig_name()
        elif self.exit_code != self.expected_exit_code:
            return self.describe_exit_code()
        else:
            return str(self)

    def describe_exit_code(self):
        msg = "Exit code was: {code}".format(code=self.exit_code)
        if self.expected_exit_code != None:
            msg += " (expected {expected_code})".format(expected_code=self.expected_exit_code)
        msg += "."

        return msg

    def sig_name(self):
        #subprocess returns signals as negative values
        sig_num = -1*self.exit_code

        #Buld the dictionary of signal numbers to names
        sig_num_to_name = dict((k, v) for v, k in reversed(sorted(signal.__dict__.items())) if v.startswith('SIG') and not v.startswith('SIG_'))

        sig_name = None
        if sig_num in sig_num_to_name:
            #Recognized signal
            sig_name = sig_num_to_name[sig_num]
        else:
            #Unkown signal
            sig_name = "Unkowwn Signal {num}".format(num=sig_num)

        return sig_name

class ConfigError(Exception):
    """
    Problem with system configuration
    """
    pass

class BuildError(Exception):
    """
    Problem building something
    """
    pass

class TimeOutError(Exception):
    """
    An action timed out
    """
    def __init__(self, message, output=None):
        super(Exception, self).__init__(message)
        self.output = output

