# This file defines basic classes used in the ece297 testing infrastructure
#
# These amount to the public interface used by scripts such as ece297exercise, ece297submit, ece297automark
#
# New types of tests are added by extending the Builder/Runner/Parser action objects through inheritance
# and constructing a new tester (e.g. BuildRunParseTester) that uses them.  See for example ece297valgrindexectest
#
# The actual test cases run are typically determined by a config file (see ece297config).
#
#
import sys
import shutil
import time

from ece297test.util import run_command, TermStyle, get_param
from ece297test.errors import ConfigError

class TestResult(object):
    """
    The result of running a single test
    """
    def __init__(self, test_name, test_time=0.0, qor=None):
        self._test_name = test_name
        self._num_failures = 0
        self._messages = [];
        self._test_time = test_time
        self._qor = qor

    def add_failure(self, failure_count=1, message=None):
        self._num_failures += failure_count
        if message != None:
            self._messages.append(message);

    def test_name(self):
        return self._test_name

    def passed(self):
        return self._num_failures == 0 

    def num_failures(self):
        return self._num_failures

    def messages(self):
        return self._messages;

    def test_time(self):
        return self._test_time

    def test_qor(self):
        return self._qor

class TesterResult(object):
    """
    The result of a Tester.  Consists of a collectino of TestResult objects
    """
    def __init__(self, tester_name, timed_out=False, failed_out=False, skipped=False):
        super(TesterResult, self).__init__()
        self._tester_name = tester_name
        self._test_results = []
        self._timed_out = timed_out
        self._failed_out = failed_out
        self._skipped = skipped

    def tester_name(self):
        return self._tester_name

    def add_test_result(self, test_result):
        self._test_results.append(test_result)

    def summary(self, f, indent="", name_width=None):
        #Allow fixed width name printing for improved readability
        if not name_width:
            name_width = len(self.tester_name())
        print("{indent}{name} ".format(indent=indent, name=self.tester_name().ljust(name_width)), end=' ', file=f)

        if self.skipped():
            print(TermStyle.SKIP + "SKIP" + TermStyle.RESET + " (Incompatible build)", file=f)
        elif self._timed_out:
            print(TermStyle.FAIL + "FAIL" + TermStyle.RESET + " (Timed Out)", file=f) 
        elif self._failed_out:
            print(TermStyle.FAIL + "FAIL" + TermStyle.RESET + " (Crashed / Invalid Result)", file=f)
        else:

            outcome = TermStyle.PASS + "PASS" + TermStyle.RESET
            if self.num_failed() > 0:
                outcome = TermStyle.FAIL + "FAIL" + TermStyle.RESET

            print("{outcome} ({failed:2} of {total:2} failed)".format(outcome=outcome,
                                                                           failed=self.num_failed(),
                                                                           total=self.num_tests()), file=f)
    def tester_results(self):
        return self._test_results

    def num_tests(self):
        return len(self._test_results)

    def num_failed(self):
        if self._timed_out or self._failed_out:
            return 1; #Ensure failure is recorded
        else:
            return len([x for x in self._test_results if not x.passed()])

    def passed(self):
        return self.num_failed() == 0

    def skipped(self):
        return self._skipped


class Tester(object):
    """
    Base class for tester objects
    """
    def __init__(self, name):
        super(Tester, self).__init__()
        self._name = name

    def name(self):
        return self._name

    def test(self, indent='', profiler=None):
        return TesterResult(tester_name=self._name)

class BuildRunParseTester(Tester):
    """
    Standard Tester which follows the typical test steps:
        1) Build
        2) Run
        3) Parse

    Each of these actions is encapsulated as a Builder/Runner/Parser
    object.

    Sub-classes MUST define the following dictionaries in their __init__ methods:
        self.builders
        self.runners
        self.parsers

    These map from the strings retrieved from the configuration file to the associated
    class.  The __init__ method (called by chaining) then constructs the appropriate
    builder/runner/parser class requested in the config file.
    
    """

    def __init__(self, config, section, defaults):
        """
            NOTE: requires base classes to fill in the
                  builders/runners/parsers dictionaries before
                  chaining to this base class!
        """
        super(BuildRunParseTester, self).__init__(name=section)

        #Identify components
        builder_type = get_param(config, section, 'builder', defaults)
        runner_type = get_param(config, section, 'runner', defaults)
        parser_type = get_param(config, section, 'parser', defaults)

        self.builder = None
        self.runner = None
        self.parser = None

        #Construct the builder
        if builder_type in self.builders:
            self.builder = self.builders[builder_type](config, section, defaults)
        else:
            raise ConfigError("Unsupported UnitTest builder type: {type} (must be one of {valid_types})".format(type=builder_type,
                                                                                                                valid_types=list(self.builders.keys())))

        #Construct the runner
        if runner_type in self.runners:
            self.runner = self.runners[runner_type](config, section, defaults)
        else:
            raise ConfigError("Unsupported UnitTest runner type: {type} (must be one of {valid_types})".format(type=runner_type,
                                                                                                               valid_types=list(self.runners.keys())))

        #Construct the parser
        if parser_type in self.parsers:
            self.parser = self.parsers[parser_type](config, section, defaults)
        else:
            raise ConfigError("Unsupported UnitTest runner type: {type} (must be one of {valid_types})".format(type=parser_type,
                                                                                                               valid_types=list(self.parsers.keys())))
    def test(self, output_f, indent='', profiler=None):

        #Build it
        print(indent + 'Building  ', self.name(), file=output_f)
        output_f.flush()
        self.builder.build()


        #Run it 
        print(indent + 'Running   ', self.name(), file=output_f)
        output_f.flush()
        start_time = time.time()
        output, returncode = self.runner.run(output_f, indent, profiler=profiler)
        end_time = time.time()

        result = None
        if returncode == None:
            #The test was skipped, don't bother parsing
            result = TesterResult(self.name(), skipped=True)
        else:
            #Parse it (not skipped)
            print(indent + 'Analyzing ', self.name(), file=output_f)
            print(2*indent + 'Testing (including setup) took: {time:.2f} sec'.format(time=(end_time - start_time)), file=output_f)
            result = self.parser.parse(self.name(), output, returncode)
            for tr in result.tester_results():
                for m in tr.messages():
                    print(2*indent + m);

        #Clean it
        self.builder.clean()

        assert result != None
        return result

class TestComponent(object):
    """
    Base class for tester actions with re-directable output
    """
    def __init__(self):
        super(TestComponent, self).__init__()

class Builder(TestComponent):
    """
    Base for performing the build action of a test
    """
    def __init__(self, config, section, defaults):
        super(Builder, self).__init__()
        self.timeout = float(get_param(config, section, 'builder_timeout', defaults))

    def build(self):
        pass

    def clean(self):
        pass

class Runner(TestComponent):
    """
    Base for performing the run action of a test
    """
    def __init__(self, config, section, defaults):
        super(Runner, self).__init__()
        self.timeout = float(get_param(config, section, 'runner_timeout', defaults))

        self.exec_file_path = get_param(config, section, 'executable_file_path', defaults, required=False)
        if not self.exec_file_path:
            self.exec_file_path = "./" + section

        self.exec_args = get_param(config, section, 'executable_arguments', defaults, required=False)
        if not self.exec_args:
            self.exec_args = []

    def run(self, output_f, indent, profiler=None):
        cmd = [self.exec_file_path] + self.exec_args

        if profiler == 'perf':
            #Wrap the command in a call to perf
            # We provide --call-graph lbr to get readable tracebacks
            # and -q to suppress any extra perf output (which can break the parsers)
            cmd = ['perf', 'record',
                   '--call-graph', 'lbr',
                   '-q',
                   '-o', 'perf.data'] + cmd

        return run_command(cmd, "Running Tests", timeout=self.timeout, f=output_f, indent=2*indent)

class Parser(TestComponent):
    """
    Base for performing the parse action of a test
    """
    def __init__(self, config, section, defaults):
        super(Parser, self).__init__()
        self.timeout = get_param(config, section, 'parser_timeout', defaults)

        #Exit code is optional
        self.expected_exit_code = get_param(config, section, 'expected_exit_code', defaults, required=False)
        if self.expected_exit_code:
            self.expected_exit_code = int(self.expected_exit_code)

    def parse(self, name, test_output, test_exit_code):
        raise NotImplementedError()
