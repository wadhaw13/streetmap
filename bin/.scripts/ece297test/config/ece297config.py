import os
import re

import configparser

from ece297test.testers import UnitTester, ExecTester, ValgrindExecTester
from ece297test.errors import ConfigError
from ece297test.util import get_param


MIN_MILESTONE_NUMBER = 0
MAX_MILESTONE_NUMBER = 5
DEFAULT_CONFIG_DIR="/cad2/ece297s/public/"

ECE297_GROUP_REGEX = re.compile(r"cd-\d\d\d")

class MilestoneConfig(object):
    """
    A configuration object which loads and parses a config file,
    and constructs the appropriate tester objects.
    """
    def __init__(self, config_dir, config_file_path, milestone_num, map_dir=None):
        super(MilestoneConfig, self).__init__()

        #Milestone configuration
        self._milestone_number = milestone_num
        self._build_command = None
        self._makefile_name = None
        self._executable_file_name = None
        self._static_library_file_names = []
        self._resources_dir = None
        self._milestone_base_name = None
        self._exercise_exec_path = None
        self._late_submission_method = None
        self._init_method = None
        self._init_base_zip_url = None
        self._init_svn_ignore_patterns = None

        #List of tester objects
        self._testers = []

        self._load_config(config_dir, config_file_path, milestone_num, map_dir)

    def milestone_number(self):
        return self._milestone_number

    def build_command(self):
        return self._build_command

    def makefile_name(self):
        return self._makefile_name

    def executable_file_name(self):
        return self._executable_file_name

    def static_library_file_names(self):
        return self._static_library_file_names

    def resources_dir(self):
        return self._resources_dir

    def milestone_base_name(self):
        return self._milestone_base_name

    def exercise_exec_path(self):
        return self._exercise_exec_path

    def testers(self):
        return self._testers

    def init_method(self):
        return self._init_method
    def init_base_zip_url(self):
        return self._init_base_zip_url

    def init_svn_ignore_patterns(self):
        return self._init_svn_ignore_patterns

    def late_submission_milestone_number(self):
        if self._late_submission_method == 'offset_by_max_milestone':
            return self._milestone_number + MAX_MILESTONE_NUMBER
        elif self._late_submission_method in ["", "none", "None", None]:
            return None
        else:
            assert "Unrecognized late submission method {method}".format(method=self._late_submission_method)

    def _load_config(self, config_dir, config_file_path, milestone_num, map_dir):
        if map_dir == None:
            map_dir = os.path.join(config_dir, 'maps')
        #Load the config
        # config_dir is used to interpolate where appropriate
        config = configparser.SafeConfigParser({'config_dir': config_dir, 'map_dir': map_dir, 'milestone_num': str(milestone_num)})
        config.read(config_file_path)

        #Ensure that all sections are uniqely named
        seen_section = {}
        for section in config.sections():
            if section in seen_section:
                raise ConfigError("Duplicate sections named '" + section + "'")
            else:
                seen_section[section] = True

        supported_test_types = {'UnitTest': UnitTester,
                                'ExecTest': ExecTester,
                                'ValgrindExecTest': ValgrindExecTester}


        if "Milestone" not in config.sections():
            raise ConfigError("Could not find required 'Milestone' section in config file.")

        if "Submit" not in config.sections():
            raise ConfigError("Could not find required 'Submit' section in config file.")

        test_defaults = {}

        for section in config.sections():
            if section == "Milestone":
                self._build_command = config.get(section, 'build_command')
                self._makefile_name = config.get(section, 'makefile_name')
                self._executable_file_name = config.get(section, 'executable_file_name')
                self._static_library_file_names = config.get(section, 'static_library_file_names').split()
                self._milestone_base_name = config.get(section, 'milestone_base_name')

                if config.has_option(section, 'resources_dir'):
                    self._resources_dir = config.get(section, 'resources_dir');

            elif section == "Init":
                if config.has_option(section, 'init_method'):
                    valid_init_methods = ['individual', 'group', 'group_update', 'none']
                    
                    init_method = config.get(section, 'init_method')
                    if init_method not in valid_init_methods:
                        assert False, "Unrecognized init method {}".format(init_method)
                    if init_method == "none":
                        self._init_method = None
                    else:
                        self._init_method = init_method
                else:
                    self._init_method = None

                if config.has_option(section, 'init_base_zip_url'):
                    self._init_base_zip_url = config.get(section, 'init_base_zip_url')
                else:
                    self._init_base_zip_url = None

                if config.has_option(section, 'init_svn_ignore_patterns'):
                    self._init_svn_ignore_patterns = config.get(section, 'init_svn_ignore_patterns').split()
                else:
                    self._init_svn_ignore_patterns = None

                #Load test building defaults

            elif section == "Submit":
                self._exercise_exec_path = config.get(section, 'exercise_exec_path')

                if config.has_option(section, 'late_submission_method'):
                    self._late_submission_method = config.get(section, 'late_submission_method')
                else:
                    self._late_submission_method = "offset_by_max_milestone"

            elif section == "Defaults":

                for param, value in config.items(section):

                    if param in test_defaults:
                        print("Warning: param {} previously had default set to {}, overwriting with new value {}".format(param, test_defaults[param], value))

                    test_defaults[param] = value
                

            else:
                test_type = get_param(config, section, 'test_type', test_defaults, required=True)
                if test_type in supported_test_types:

                    try:
                        #Will create a tester object based on the sections config
                        # and add it to the self.testers list
                        tester = supported_test_types[test_type](config, section, test_defaults)

                    except configparser.NoOptionError as e:
                        raise ConfigError("Missing required config-file parameter. {msg}".format(msg=str(e)))

                    #Add to list
                    self._testers.append(tester)

                else:
                    raise ConfigError("Unsupported test type {type} (must be one of {valid_types})".format(type=test_type, valid_types=list(supported_test_types.keys())))
