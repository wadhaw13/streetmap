#!/usr/bin/python3

import os
from os.path import join as path_join
from os.path import expanduser
import sys
import fnmatch
import re
import tempfile
import filecmp

#Argument parsing
import argparse

#Python interface to SVN

import zipfile
import shutil
import getpass
import grp


from ece297test.errors import ActionFailedError
from ece297test.config import MilestoneConfig, MIN_MILESTONE_NUMBER, MAX_MILESTONE_NUMBER, DEFAULT_CONFIG_DIR
from ece297test.util import mkdir_p, get_url, TermStyle, find_config_file, run_command, run_essential_command, prompt_user_yes_no

STUDENT_297_BASE_DIR = "~/ece297"
INDIVIDUAL_BASE_REPO_DIR = "~/ece297/repos"
GROUP_BASE_REPO_DIR = "/groups/ECE297S/"
WORKING_BASE_DIR = "~/ece297/work"
REPO_HOST = "ug251.eecg.utoronto.ca:" # not needed... except to work-around some bugs in Netbeans

PROJECT_NAME="mapper"

def main():
    """
    Initialization script for ECE297 milestones

    """

    #Parse args, set defaults
    args = parse_args()

    if args.color == 'true' or (args.color == 'auto' and sys.stdout.isatty()):
        TermStyle.enable()
    else:
        TermStyle.disable()

    #Load the config
    milestone_config = MilestoneConfig(args.config_dir, find_config_file(args.config_dir, args.milestone_number), args.milestone_number)

    try:
        if milestone_config.init_method() == 'individual':
            individual_init(args, milestone_config)
        elif milestone_config.init_method() == 'group':
            group_init(args, milestone_config)
        elif milestone_config.init_method() == 'group_update':
            group_update(args, milestone_config)
        elif milestone_config.init_method() == None:
            print("No initialization required for this milestone!")
            return 1
        else:
            print("Unrecognized configuration method:", mielston_config.init_method())
            return 2
    except ActionFailedError as e:
        print(e)
        return 3
    return 0

def parse_args():
    """
    Parses commandline arguments
    """
    description = """ Milestone initialization script for ECE297 """

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('milestone_number',
                        type=int,
                        help="The milestone number being initialized.")

    parser.add_argument('--group',
                        default=None,
                        help="Overrides default ECE297 group (cd-XXX)")

    parser.add_argument('--color',
                        choices=['auto', 'true', 'false'],
                        default='auto',
                        help=argparse.SUPPRESS)

    parser.add_argument('--config_dir',
                        type=str,
                        default=DEFAULT_CONFIG_DIR,
                        help=argparse.SUPPRESS)

    #Force the script to run, even if the target directories already
    #exist. (The previous directories will be renamed and 'hidden',
    #so as not to loose data).
    parser.add_argument('--force',
                        default=False,
                        action='store_true',
                        help=argparse.SUPPRESS)

    #Force the re-creation of the repo for group init methods
    parser.add_argument('--repo',
                        default=False,
                        action='store_true',
                        help=argparse.SUPPRESS)

    args = parser.parse_args()

    args = set_default_args(args)

    return args

def set_default_args(args):
    """
    Set default arguments in args
    """

    if not args.config_dir:
        args.config_dir = DEFAULT_CONFIG_DIR

    args.config_dir = os.path.abspath(args.config_dir)

    return args

def individual_init(args, milestone):
    repo_path = expanduser(path_join(INDIVIDUAL_BASE_REPO_DIR, "milestone{}_repo".format(milestone.milestone_number())))
    working_copy_path = expanduser(path_join(WORKING_BASE_DIR, "milestone{}".format(milestone.milestone_number())))

    if os.path.exists(repo_path):
        removed = False
        if args.force:
            removed = prompt_rmtree("Are you sure you want to remove repository {} and its subdirectories? Any data will be lost!".format(repo_path), repo_path)

        if not removed:
            raise ActionFailedError("Remote Repository path {} already exists. Will not initialize".format(repo_path))

    run_essential_command(['git', 'init', '--bare', '--shared=false', '--', repo_path], "Creating private remote Git repository at {}".format(repo_path))

    # Make sure no one else can read or execute through this directory
    # as student home directories are readable and executable by the world
    # by default on the UG machines.
    run_essential_command(['chmod', '700', expanduser(STUDENT_297_BASE_DIR)], "Making your 297 files private")

    create_working_copy(args, milestone, working_copy_path, repo_path)
    os.chdir(working_copy_path)
    make_and_push_initial_commit(milestone)

    print("DONE")
    print("")
    print("You can now open your Netbeans project at:")
    print("\t", working_copy_path)


def group_init(args, milestone):
    group = get_ece297_group(args)

    print("Found membership in group {}".format(group))

    repo_path = expanduser(path_join(GROUP_BASE_REPO_DIR, group, "{}_repo".format(PROJECT_NAME)))
    working_copy_path = expanduser(path_join(WORKING_BASE_DIR, PROJECT_NAME))

    if args.repo and os.path.exists(repo_path):
        removed = False
        if args.force:
            removed = prompt_rmtree("Are you sure you want to remove repository {} and its subdirectories? Any data will be lost!".format(repo_path), repo_path)

        if not removed:
            raise ActionFailedError("Remote Repository path {} already exists. Will not initialize".format(repo_path))

    is_first_user = not os.path.exists(repo_path) or args.repo;

    if is_first_user:
        run_essential_command(['git', 'init', '--bare', '--shared=group', '--', repo_path], "Creating shared remote Git repository at {}".format(repo_path))
        run_command(['git', 'config', '--unset', 'receive.denyNonFastforwards'], "Allowing force pushes", cwd=repo_path); # --shared disables force pushes
    else:
        print("Found Existing Remote Repository at {}".format(repo_path))

    create_working_copy(args, milestone, working_copy_path, repo_path)

    if is_first_user:
        os.chdir(working_copy_path)
        make_and_push_initial_commit(milestone)

    print("DONE")
    print("")
    print("You can now open your Netbeans or Eclipse project at:")
    print("\t", working_copy_path)

def group_update(args, milestone):
    working_copy_path = expanduser(path_join(WORKING_BASE_DIR, PROJECT_NAME))

    if not os.path.exists(working_copy_path):
        raise ActionFailedError("No working copy path {} found".format(working_copy_path))
    if not os.path.isdir(working_copy_path):
        raise ActionFailedError("Working copy path {} not a directory".format(working_copy_path))

    os.chdir(working_copy_path)

    base_zip_file = ".tmp.zip"
    try:
        get_url(milestone.init_base_zip_url(), base_zip_file)

        #Unzip the base file
        print("Extracting Update Files...")
        create_or_update_ignore_patterns(milestone);

        with zipfile.ZipFile(base_zip_file) as zip_f:
            prompt_extract_if_different(milestone, zip_f)

    finally:
        #Remove the downloaded zip
        os.remove(base_zip_file)

    print("DONE")
    print("")
    print("Your Netbeans project is now up-to-date for M{} at:".format(milestone.milestone_number()))
    print("\t", working_copy_path)

def prompt_extract_if_different(milestone, zip_f):
    """
    Extracts the opened zip file zip_f
    into the current directory if:
        1) The file differs from the current on-disk file (or the on-disk file doesn't exist)
        2) The user confirms they want to overwrite
    """

    print("Making sure you are up-to-date with your remote repository")
    run_essential_command(['git', 'fetch', '--all'], "Syncing local info for remote repository");

    output = run_essential_command(['git', 'status', '--porcelain=2', '--branch'], "Figuring out upstream branch name", live_output=False);
    upstream_branch_name = None
    for line in output[0]:
        split_line = line.split(' ');
        if (split_line[1] == "branch.upstream"):
            upstream_branch_name = split_line[2]

    if (not upstream_branch_name):
        raise ActionFailedError("Unable to determine upstream branch name. Checkout master, or set the upstream for this branch");

    print(upstream_branch_name);

    remote_master_commit_hash = run_essential_command(['git', 'rev-parse', upstream_branch_name],   "Getting remote commit ID");
    local_current_commit_hash = run_essential_command(['git', 'rev-parse', 'HEAD'],                 "Getting current commit ID");

    if (remote_master_commit_hash != local_current_commit_hash):
        raise ActionFailedError("Not up-to-date with remote repository. Pull the latest changes (or push yours) and try again");

    #We initially extract the whole zip file to a temporary directory
    tmp_dir = tempfile.mkdtemp(suffix='ece297init')

    try:
        filenames = [info.filename for info in zip_f.infolist() if not info.filename.endswith('/')]


        zip_f.extractall(tmp_dir)

        matching_paths, differing_paths, uncomparable_paths = filecmp.cmpfiles(tmp_dir, '.', filenames, shallow=False)

        overwrite_modified_files = False
        if len(differing_paths) != 0 or len(uncomparable_paths) != 0:

            differing_files = [p for p in differing_paths if os.path.isfile(p)]
            if len(differing_files) != 0:
                msg = 'The following files have local modifications:\n'
                for f in differing_files:
                    msg += '\t' + f + '\n'
                print(msg)

                overwrite_modified_files = prompt_user_yes_no("Are you sure you want to overwrite these files? Any modifications will be lost!", default=None)

                if not overwrite_modified_files:
                    raise ActionFailedError('Making no modifications')

            missing_files = [p for p in uncomparable_paths if not os.path.exists(p)]
            if len(uncomparable_paths) != 0:
                for p in uncomparable_paths:
                    if os.path.exists(p):
                        if os.path.isfile(p):
                            raise ActionFailedError('File {} exists but is not comparable'.format(p))
                        else:
                            assert os.path.isdir(p)

            #Files are either missing, or modified (and we want to overwrite)

            #Copy any missing files
            for file in missing_files:
                dirname = os.path.dirname(file)
                if (len(dirname)!=0):
                    mkdir_p(dirname)
                src = os.path.join(tmp_dir, file)
                dst = file
                shutil.copy(src, dst)

            #Overwrite any differeing files
            for file in differing_files:
                assert overwrite_modified_files
                dirname = os.path.dirname(file)
                if (len(dirname)!=0):
                    mkdir_p(dirname)
                src = os.path.join(tmp_dir, file)
                dst = file
                shutil.copy(src, dst)

            try:
                print("Adding files for milestone update commit")
                for file in missing_files + differing_files + [".gitignore"]:
                    run_essential_command(['git', 'add', '--', file], "Add {} to Git".format(file))

                commit_message = "Update for Milestone {} by {} for {}".format(milestone.milestone_number(), sys.argv[0], getpass.getuser());
                run_essential_command(['git', 'commit', '-m', commit_message], "Milestone Update Commit")
            except:
                run_command(['git', 'reset', '.'], "Trying to reset index due to command failure".format(file, show_action_name=True))
                raise;

            run_essential_command(['git', 'push'], "Pushing milestone update commit")

        else:
            assert len(uncomparable_paths) == 0
            assert len(differing_paths) == 0
            assert len(matching_paths) == len(filenames)
            # TODO: If certain git commands fail, then we will be left with a bunch of files. Probably want to remove them.
            print("Existing files already up-to-date -- No changes necessary")

    finally:
        shutil.rmtree(tmp_dir) #Clean-up

def create_working_copy(args, milestone, working_copy_path, repo_path):
    if os.path.exists(working_copy_path):
        removed = False
        if args.force:
            removed = prompt_rmtree("Are you sure you want to remove working copy {} and its subdirectories? Any data will be lost!".format(working_copy_path), working_copy_path)

        if not removed:
            if args.force:
                create_nbproject(milestone, working_copy_path, force=True);
            raise ActionFailedError("Working copy path {} already exists. Will not initialize further".format(working_copy_path));

    # add a host, forcing the ssh protocol so that Netbeans behaves properly - it messes up permissions when using the file protocol.
    repo_path_with_host = REPO_HOST + repo_path
    run_essential_command(['git', 'clone', '--', repo_path_with_host, working_copy_path], "Clone Git repository from {} into {}".format(repo_path_with_host, working_copy_path))
    create_nbproject(milestone, working_copy_path, force=False);

def make_and_push_initial_commit(milestone):
    """
    Creates the initial commit, and pushes it up to the repository
    Assumes PWD is in working copy
    """

    print("Extracting Base Files...")
    extract_milestone_files(milestone)
    create_or_update_ignore_patterns(milestone);

    commit_message = "Initial Milestone {} commit by {} for {}".format(milestone.milestone_number(), os.path.realpath(sys.argv[0]), getpass.getuser());
    try:
        run_essential_command(['git', 'add', '.'],                         "Add initial commit files",   );
        run_essential_command(['git', 'commit', '-m', commit_message],     "Make initial commit",        );
    except:
        run_command(['git', 'reset', '.'], "Trying to reset index due to command failure", show_action_name=True);
        raise;

    run_essential_command(['git', 'push'],                           "Push initial commit");

    # The ~/ece297 base directory should already be private, but
    # make the working copy base directory private too for safety.
    run_essential_command(['chmod', '700', expanduser(WORKING_BASE_DIR)], "Making your working directory private")


def create_or_update_ignore_patterns(milestone):
    with open(".gitignore", 'a+') as gi_f:
        existing_patterns = {line.split('#')[0].strip() for line in gi_f}
        missing_ignore_patterns = [pat for pat in milestone.init_svn_ignore_patterns() if pat not in existing_patterns]
        gi_f.writelines([l + '\n' for l in missing_ignore_patterns]);

def extract_milestone_files(milestone, path='.', include_nbproject=False):
    #Download the base file
    base_zip_file = ".tmp.zip"
    get_url(milestone.init_base_zip_url(), base_zip_file)

    #Unzip the base file
    with zipfile.ZipFile(base_zip_file) as zip_f:
        zip_f.extractall(path=path)

    #Remove the downloaded zip
    os.remove(base_zip_file)

    exclude_dirs=[]
    if not include_nbproject:
        exclude_dirs += ['nbproject'];

    #Remove the intermediate dir if it exists
    for prefix in ["assignment{}".format(milestone.milestone_number()), "milestone{}".format(milestone.milestone_number()), "mapper"]:
        intermediate_dir = path_join(path, prefix);
        if os.path.exists(intermediate_dir):
            for filename in os.listdir(intermediate_dir):
                if filename in exclude_dirs:
                    continue;
                shutil.move(os.path.join(intermediate_dir, filename), path)
            shutil.rmtree(intermediate_dir)
            break

def create_nbproject(milestone, working_copy_path, force=False):
    dest_location = path_join(working_copy_path, 'nbproject');
    print("Trying to make nbproject at {}".format(dest_location));

    if os.path.exists(dest_location):
        if force:
            removed = prompt_rmtree("Are you sure you want to reset all netbeans configuration at {} ?".format(dest_location), dest_location);
            if not removed:
                return False;
        else:
            print("Netbeans project {} already exists. Refusing to overwrite".format(dest_location));
            return False;

    td = None;
    try:
        td = tempfile.mkdtemp()
        extract_milestone_files(milestone, path=td, include_nbproject=True)
        src_location = path_join(td, 'nbproject');
        if os.path.exists(src_location):
            shutil.copytree(path_join(td, 'nbproject'), dest_location);
        else:
            print("No netbeans project for this milestone");
    finally:
        if td is not None:
            shutil.rmtree(td);

    return True;

def prompt_rmtree(prompt, path):
    """
    This doesn't really remove, only 'hides' the original directory
    specified by path (this ensures student's shouldn't loose data, 
    even if they forcibly removed it)
    """

    dirname = os.path.dirname(path)
    basename = os.path.basename(path)

    #Find a unique 'hidden' name
    cnt = 0
    new_path = None
    while True:
        new_basename = "." + basename + "_old{:03d}".format(cnt)
        new_path = os.path.join(dirname, new_basename)

        if not os.path.exists(new_path):
            break
        cnt += 1
    

    if prompt_user_yes_no(prompt, default=None):
        shutil.move(path, new_path)
        return True
    else:
        return False


def get_ece297_group(args):
    """
    Finds a users ECE297 group (cd-XXX, where XXX is an integer)
    """
    
    #Overriden
    if args.group != None:
        return args.group

    #Look-up
    all_groups = [grp.getgrgid(g).gr_name for g in os.getgroups()]

    ece297_grp_regex = re.compile(r"cd-\d\d\d")

    matching_groups = []
    for group in all_groups:
        match = ece297_grp_regex.match(group)
        if match:
            matching_groups.append(group)

    if len(matching_groups) == 0:
        raise ActionFailedError("No ECE297 group associated with this username")
    elif len(matching_groups) > 1:
        raise ActionFailedError("Multiple ECE297 groups associated with this username ({}). Override --groups option".format(", ".join(matching_groups)))
    else:
        assert len(matching_groups) == 1
        return matching_groups[0]



if __name__ == '__main__':
    sys.exit(main())
